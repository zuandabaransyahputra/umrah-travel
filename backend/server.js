import express from 'express'
import dotenv from 'dotenv'
import { notFound, errorHandler } from './middleware/errorMiddleware.js'
import userRoutes from './routes/userRoutes.js'
import connectDB from './config/db.js'
import path from 'path'

//Config env
dotenv.config()

//connect to database
connectDB()

//Express App
const app = express()

//Body parser
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

//routes
app.use('/api/users', userRoutes)

//Static file
const __dirname = path.resolve()
app.use('/uploads', express.static(path.join(__dirname, '/uploads')))

if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, '/frontend/build')))

    app.get('*', (req, res) => res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html')))
} else {
    app.get('/', (req, res) => {
        res.send('API is running...')
    })
}

//error middleware
app.use(notFound)
app.use(errorHandler)

//Server
const port = process.env.PORT || 5000
app.listen(port, () => {
    console.log(`Berhasil menjalankan server pada port: ${port}`)
})