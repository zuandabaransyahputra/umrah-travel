import asyncHandler from 'express-async-handler'
import User from '../models/userModel.js'
import generateToken from '../utils/generateToken.js'

const registerUser = asyncHandler(async (req, res) => {
    const { name, email, password, address } = req.body
    const userExist = await User.findOne({ email })
    if (userExist) {
        res.status(400)
        throw new Error('Mohon maaf user telah terdaftar')
    }
    const user = await User.create({
        name,
        email,
        password,
        address
    })
    if (user) {
        res.status(201).json({
            message: "Berhasil Mendaftar, Silahkan Login"
        })
    } else {
        res.status(400)
        throw new Error('User tidak ditemukan')
    }
})

const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body

    const user = await User.findOne({ email })

    if (user && (await user.matchPassword(password))) {
        return res.json({
            _id: user.id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            image: user.image,
            address: user.address,
            token: generateToken(user._id)
        })
    } else {
        res.status(401)
        throw new Error('Email atau Password Salah')
    }
})


export { registerUser, loginUser }