import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { login } from '../../actions/userActions';
import { ToastContainer, toast } from 'react-toastify';
import Loader from '../../components/loader/Loader';
import Message from '../../components/message/Message';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userRegister = useSelector(state => state.userRegister);
  const { success: successRegister } = userRegister;

  const userLogin = useSelector(state => state.userLogin);
  const { loading, error, userInfoTravel } = userLogin;

  useMemo(() => {
    if (successRegister) {
      toast.success(successRegister.message);
    }
  }, [successRegister])

  useEffect(() => {
    if (userInfoTravel) {
      navigate('/');
    }
  }, [navigate, userInfoTravel]);

  const submitHandler = e => {
    e.preventDefault();
    dispatch(login(email, password));
  };

  return (
    <>
      <ToastContainer />
      <div className="min-h-screen flex flex-col mb-[-5rem]">
        <div className="container max-w-md mx-auto flex-1 flex flex-col items-center justify-center px-2">
          {loading && <Loader />}
          {error && <Message message={error} color={'red'} />}
          <div className="bg-white px-6 py-8 rounded shadow-md text-secondary w-full">
            <h1 className="mb-8 text-3xl text-center text-color font-bold">
              Login Akun
            </h1>
            <form onSubmit={submitHandler} method="post">
              <input
                type="email"
                className="block border border-grey-light w-full p-3 rounded mb-4 focus:outline-primary"
                name="email"
                placeholder="Email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
              />
              <input
                type="password"
                className="block border border-grey-light w-full p-3 rounded mb-4 focus:outline-primary"
                name="password"
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                required
              />
              <button
                type="submit"
                className="w-full text-center py-3 rounded bg-primary text-white focus:outline-none my-1"
              >
                Submit
              </button>
            </form>
          </div>

          <div className="text-color mt-6">
            Belum memiliki akun?
            <Link className="text-md text-secondary font-bold" to="/register">
              {' '}
              Daftar
            </Link>
            .
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
