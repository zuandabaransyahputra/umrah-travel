import React from 'react';
import { Link } from 'react-router-dom';
import Card from '../../components/card/Card';
import Testimonial from '../../components/testimonial/Testimonial';
import Experience from '../../parts/experience/Experience';
import Hero from '../../parts/hero/Hero';
import { useSelector } from 'react-redux';

const Home = () => {
  const userLogin = useSelector(state => state.userLogin);
  const { userInfoTravel } = userLogin;

  return (
    <>
      <Hero />
      <main>
        <section className="mt-[-50px] flex">
          <Experience />
        </section>
        <section className="min-h-[540px] bg-secondary mb-[-230px] mt-[-50px]">
          <div className="flex items-center justify-center">
            <div className="mt-[150px] text-center">
              <h1 className="text-3xl md:text-4xl font-bold text-white">
                Jadwal Umroh
              </h1>
              <p className="text-md font-[300] text-gray-200 mt-2">
                Pesan tiket sekarang juga <br /> sebelum kehabisan
              </p>
            </div>
          </div>
        </section>
        <section className="w-full md:w-[90%] md:mx-auto flex">
          <Card />
        </section>
        <section className="mt-[-150px] pt-[220px] bg-gray-50 pb-[50px]">
          <div className="w-[90%] mx-auto">
            <div className="md:w-[90%] mx-auto flex-1 flex flex-col lg:flex-row items-start lg:items-center justify-between">
              <div className="w-full flex flex-col items-center lg:items-start md:justify-start text-secondary">
                <h1 className="text-3xl md:text-4xl font-bold">Our Networks</h1>
                <p className="text-center lg:text-left text-md font-[300] mt-2">
                  Perusahaan yang bekerjasama <br /> dengan kami
                </p>
              </div>
              <div className="w-full mt-2 lg:flex-[2] flex flex-wrap md:flex-nowrap items-center justify-around lg:justify-end h-full">
                <img
                  src={process.env.PUBLIC_URL + 'images/ana.png'}
                  alt="Ana"
                  className="max-w-[100px] md:max-w-[150px]"
                />
                <img
                  src={process.env.PUBLIC_URL + 'images/disney.png'}
                  alt="Disney"
                  className="max-w-[100px] md:max-w-[150px]"
                />
                <img
                  src={process.env.PUBLIC_URL + 'images/shangri-la.png'}
                  alt="Shangri"
                  className="max-w-[100px] md:max-w-[150px]"
                />
                <img
                  src={process.env.PUBLIC_URL + 'images/visa.png'}
                  alt="Visa"
                  className="max-w-[100px] md:max-w-[150px]"
                />
              </div>
            </div>
          </div>
        </section>
        <section className="bg-gray-50 min-h-[450px] md:min-h-[500px]">
          <div className="flex flex-col items-center justify-start">
            <div className="flex flex-col">
              <h1 className="text-center text-3xl text-secondary font-bold">
                Kata Mereka Tentang Kami
              </h1>
              <p className="text-md text-secondary font-[300] text-center">
                Testimonial mereka yang pernah <br /> memilih kami untuk umrah
              </p>
            </div>
          </div>
        </section>
        <section className="mt-[-250px] md:mt-[-300px]">
          <Testimonial />
        </section>
        <section>
          <div className="flex flex-wrap gap-5 items-center justify-center my-20">
            <Link
              to="/"
              className="bg-gray-200 p-4 text-gray-400 font-[400] rounded-md"
            >
              Butuh Bantuan
            </Link>
            {!userInfoTravel && (
              <Link
                to="/register"
                className="bg-primary p-4 text-white font-[400] rounded-md"
              >
                Daftar Sekarang
              </Link>
            )}
          </div>
        </section>
      </main>
    </>
  );
};

export default Home;
