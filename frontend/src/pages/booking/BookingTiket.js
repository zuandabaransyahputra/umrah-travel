import React from 'react';

const BookingTiket = () => {
  return (
    <>
      <section className="bg-gray-200 min-h-[400px]">
        <div className="flex items-start justify-start">
          <div className="w-[90%] mx-auto mt-28">
            <p className="text-secondary font-400 text-md  ">Booking Tiket</p>
          </div>
        </div>
      </section>
      <section className="mb-20 mt-[-220px] w-[90%] mx-auto">
        <div className="flex flex-col lg:flex-row gap-8">
          <div className="flex-1 lg:flex-[2] border-[1px] border-gray-200 p-6 rounded-lg bg-white">
            Hello
          </div>
          <div className="flex-1 flex flex-col gap-4 h-[450px] border-[1px] border-gray-200 p-6 rounded-lg bg-white">
            Hello
          </div>
        </div>
      </section>
    </>
  );
};

export default BookingTiket;
