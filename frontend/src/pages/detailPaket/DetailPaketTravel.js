import React from 'react';
import { Link, useParams } from 'react-router-dom';

const datas = [
  {
    id: 1,
    image: '/images/avatar.jpg',
    nama: 'Fulan bin Fulan',
  },
  {
    id: 2,
    image: '/images/avatar.jpg',
    nama: 'Fulan bin Fulan',
  },
  {
    id: 3,
    image: '/images/avatar.jpg',
    nama: 'Fulan bin Fulan',
  },
  {
    id: 4,
    image: '/images/avatar.jpg',
    nama: 'Fulan bin Fulan',
  },
  {
    id: 5,
    image: '/images/avatar.jpg',
    nama: 'Fulan bin Fulan',
  },
];

const DetailPaketTravel = () => {
  const { id } = useParams();

  return (
    <>
      <section className="bg-gray-200 min-h-[400px]">
        <div className="flex items-start justify-start">
          <div className="w-[90%] mx-auto mt-28">
            <p className="text-secondary font-400 text-md  ">
              Umroh Bulan September
            </p>
          </div>
        </div>
      </section>
      <section className="mb-20 mt-[-220px] w-[90%] mx-auto">
        <div className="flex flex-col lg:flex-row gap-8">
          <div className="flex-1 lg:flex-[2] border-[1px] border-gray-200 p-6 rounded-lg bg-white">
            <h2 className="font-400 text-lg text-secondary mb-2">
              Umroh bersama Ustadz Fulan bin Fulan
            </h2>
            <p className="text-md font-300 text-gray-400 mb-4">
              Mekkah - Madinah - Palestina
            </p>
            <img
              src={'/images/bg-home.jpg'}
              className="w-full"
              alt="Details travel"
            />
            <h2 className="font-400 text-lg text-secondary my-2">
              Tentang Keberangkatan
            </h2>
            <p className="text-gray-400 text-sm">
              Keberangkatan akan dibimbing oleh ustadz Fulan bin Fulan.
              Keberangkatan dimulai dari kota jamaah menuju Jakarta, di Jakarta
              jamaah akan dibimbing oleh Ustadz Fulan bin Fulan tentang manasik
              umrah. Kemudian keberangkatan selanjutnya adalah dari Jakarta
              menuju Mekkah. Setelah sampai di Mekkah, para jamaah langsung
              dibawa ke tempat penginapan yang tidak jauh dari Ka'bah. Para
              jamaah akan berada di Mekkah selama 10 hari, setelah 10 hari para
              jamaah akan diberangkatkan ke kota Madinah untuk melanjutkan
              proses Umrah nya. Sesampai Madinah para jamaah akan dibawa ke
              penginapan yang tidak jauh dari Masjid Nabawi.
            </p>
            <p className="text-gray-400 mt-4 text-sm">
              Bagi jamaah yang ingin melakukan muqobbalah untuk ujian tes masuk
              ke Universitas Madinah, Ustadz akan membimbing dan membantu jamaah
              dalam melakukan proses wawancara. Setelah selesai semua proses
              Umrah, kemudian para jamaah akan diberangkatkan ke Palestina untuk
              berziarah ke Masjid Al Aqsa. Setelah semua kegiatan selesai, para
              jamaah akan diberangkatkan kembali ke Jakarta, sampai akhirnya
              para jamaah akan dikembalikan ke kotanya masing-masing.
            </p>
            <div className="flex flex-col md:flex-row items-start md:items-center mt-6 gap-10">
              <div className="flex gap-4 items-center">
                <div className="rounded-full w-[40px] h-[40px] bg-primary flex items-center justify-center text-[12px]">
                  Icon
                </div>
                <div className="flex flex-col">
                  <h2 className="text-md font-400 text-secondary">Fasilitas</h2>
                  <p className="text-sm font-300 text-gray-400">
                    Free Makan Siang
                  </p>
                </div>
              </div>
              <div className="flex gap-4 items-center">
                <div className="rounded-full w-[40px] h-[40px] bg-primary flex items-center justify-center text-[12px]">
                  Icon
                </div>
                <div className="flex flex-col">
                  <h2 className="text-md font-400 text-secondary">
                    Maskapai Penerbangan
                  </h2>
                  <p className="text-sm font-300 text-gray-400">
                    Saudi Air Lines
                  </p>
                </div>
              </div>
              <div className="flex gap-4 items-center">
                <div className="rounded-full w-[40px] h-[40px] bg-primary flex items-center justify-center text-[12px]">
                  Icon
                </div>
                <div className="flex flex-col">
                  <h2 className="text-md font-400 text-secondary">Hotel</h2>
                  <p className="text-sm font-300 text-gray-400">Bintang 5</p>
                </div>
              </div>
            </div>
          </div>
          <div className="flex-1 flex flex-col gap-4 h-[470px] border-[1px] border-gray-200 p-6 rounded-lg bg-white">
            <div className="flex flex-col items-start justify-center gap-2 border-b-[1px] border-gray-200">
              <p className="text-lg font-300 text-secondary">Members</p>
              <div className="w-full flex items-center justify-between mb-3">
                <div className="flex gap-2">
                  {datas.map(data => (
                    <img
                      key={data.id}
                      src={`${data.image}`}
                      alt={`${data.nama}`}
                      className="rounded-full w-[35px] h-[35px]"
                    />
                  ))}
                  <div className="w-[35px] h-[35px] rounded-full text-white bg-primary flex items-center justify-center text-sm ">
                    {datas.length}+
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-2">
              <h2>Informasi Keberangkatan</h2>
              <div className="flex justify-between items-center text-gray-400">
                <h2 className="font-300 text-md ">Jadwal</h2>
                <p className="font-300 text-md ">12 September 2022</p>
              </div>
              <div className="flex justify-between items-center text-gray-400">
                <h2 className="font-300 text-md ">Durasi</h2>
                <p className="font-300 text-md ">30 hari</p>
              </div>
              <div className="flex justify-between items-center text-gray-400">
                <h2 className="font-300 text-md ">Total Pax</h2>
                <p className="font-300 text-md ">30 orang</p>
              </div>
              <div className="flex justify-between items-center text-gray-400">
                <h2 className="font-300 text-md ">Sisa Pax</h2>
                <p className="font-300 text-md ">{30 - datas.length} orang</p>
              </div>
              <div className="flex justify-between items-center text-gray-400">
                <h2 className="font-300 text-md ">Berangkat Dari</h2>
                <p className="font-300 text-md ">Jakarta</p>
              </div>
              <div className="flex justify-between items-center text-gray-400 font-300 text-md ">
                <h2 className="">Harga</h2>
                <p className="">Rp 25.000.000</p>
              </div>
            </div>
            <Link
              to={`/bookings/${id}`}
              className="bg-primary py-2 px-4 text-white rounded-md text-center"
            >
              Booking Tiket
            </Link>
            <Link
              to={`/pesan/${id}`}
              className="bg-secondary py-2 px-4 text-white rounded-md text-center"
            >
              Tanya Admin
            </Link>
          </div>
        </div>
      </section>
    </>
  );
};

export default DetailPaketTravel;
