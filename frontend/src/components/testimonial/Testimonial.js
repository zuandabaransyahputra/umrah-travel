import React from 'react';

const datas = [
  {
    id: 1,
    image: process.env.PUBLIC_URL + 'images/avatar.jpg',
    name: 'Zuanda Putra',
    deskripsi:
      'Umrah yang sangat nyaman, dengan pelayanan terbaik dan tutor terbaik',
    jadwal: '12 JANUARI 2022',
  },
  {
    id: 2,
    image: process.env.PUBLIC_URL + 'images/avatar.jpg',
    name: 'Ahmad',
    deskripsi:
      'Umrah yang sangat nyaman, dengan pelayanan terbaik dan tutor terbaik',
    jadwal: '12 FEBRUARI 2022',
  },
  {
    id: 3,
    image: process.env.PUBLIC_URL + 'images/avatar.jpg',
    name: 'Zikri',
    deskripsi:
      'Umrah yang sangat nyaman, dengan pelayanan terbaik dan tutor terbaik',
    jadwal: '12 MARET 2022',
  },
];

const Testimonial = () => {
  return (
    <div className="w-[90%] flex flex-wrap items-center justify-start gap-5 mx-auto">
      {datas.map(data => (
        <div
          className="bg-white flex-1 flex flex-col gap-4 items-center justify-between min-h-[350px] md:min-h-[420px] min-w-full md:min-w-[200px] p-4 border-[1px] border-gray-200"
          key={data.id}
        >
          <div className="flex flex-col items-center justify-center text-white">
            <img
              src={data.image}
              alt={data.name}
              className="rounded-full w-[75px] h-[75px] md:w-[120px] md:h-[120px]"
            />
            <p className="text-sm font-[300] text-secondary my-4">
              {data.name}
            </p>
            <h1 className="text-[16px] md:text-[18px] text-center font-400 text-black">
              "{data.deskripsi}"
            </h1>
          </div>
          <div className="flex">
            <p className="text-center">{data.jadwal}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Testimonial;
