import { Link } from 'react-router-dom';

const datas = [
  {
    id: 1,
    image: '/images/card-1.jpg',
    tanggal: '12 JUNI 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 2,
    image: '/images/card-2.jpg',
    tanggal: '12 JULI 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 3,
    image: '/images/card-3.jpg',
    tanggal: '12 AGUSTUS 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 4,
    image: '/images/card-4.jpg',
    tanggal: '12 SEPTEMBER 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
];

const Card = () => {
  return (
    <div className="w-[90%] mx-auto flex items-center justify-center gap-2 md:gap-4 lg:gap-6 flex-wrap">
      {datas.map(data => (
        <div
          className="flex-1 rounded-md flex flex-col items-center justify-between min-h-[380px] min-w-[250px] bg-cover bg-center p-4"
          style={{
            backgroundImage: `url(${data.image})`,
          }}
          key={data.id}
        >
          <div className="flex flex-col items-center justify-center text-white">
            <p className="text-md lg:text-sm font-[300]">{data.tanggal}</p>
            <h1 className="text-[18px] md:text-[16px] font-bold text-center">{data.tutor}</h1>
          </div>
          <Link
            to={`/paket-travel/${data.id}`}
            className="bg-primary px-4 py-2 text-white rounded-md font-[400] text-xl md:text-sm"
          >
            Details
          </Link>
        </div>
      ))}
    </div>
  );
};

export default Card;
