import { Link, NavLink, useNavigate, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../../actions/userActions';
import React, { Fragment } from 'react';
import { Popover, Transition } from '@headlessui/react';
import { MenuIcon, XIcon } from '@heroicons/react/outline';

const Navbar = () => {
  const userLogin = useSelector(state => state.userLogin);
  const { userInfoTravel } = userLogin;

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const logoutHandler = () => {
    dispatch(logout());
    navigate('/');
  };

  return (
    <Popover>
      <div
        className={`w-[90%] mx-auto h-[10vh] absolute left-0 top-0 right-0 bottom-0 z-20 ${location.pathname === `/`
          ? 'bg-white text-secondary'
          : 'bg-secondary text-white'
          }`}
      >
        <div className="w-[90%] mx-auto h-full flex items-center justify-between">
          <Link className="text-3xl" to='/'>Logo</Link>
          <div className="-mr-2 flex items-center md:hidden">
            <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-secondary hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-primary">
              <span className="sr-only">Open main menu</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>
          <div className="navbar flex-[3] hidden md:flex md:justify-end md:items-center gap-10 mr-4">
            <NavLink to="/">Home</NavLink>
            <NavLink to="/paket-travel">Paket Travel</NavLink>
            <NavLink to="/services">Services</NavLink>
            <NavLink to="/testimonials">Testimonial</NavLink>
            <div>
              {userInfoTravel ? (
                <button
                  className="bg-primary py-2 px-4 text-white rounded-md"
                  onClick={logoutHandler}
                >
                  Log out
                </button>
              ) : (
                <Link
                  to="/login"
                  className="bg-primary py-2 px-4 text-white rounded-md"
                >
                  Login
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
      <Transition
        as={Fragment}
        enter="duration-150 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel
          focus
          className="absolute top-0 z-[100] inset-x-0 transition transform origin-top-right md:hidden w-[90%] h-[300px] mx-auto"
        >
          <div className="shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden w-full">
            <div className="w-[90%] mx-auto flex items-center justify-between h-[75px]">
              <div>
                <Link className="text-3xl text-secondary" to='/'>Logo</Link>
              </div>
              <div className="-mr-2">
                <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-secondary hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-primary">
                  <span className="sr-only">Close main menu</span>
                  <XIcon className="h-6 w-6" aria-hidden="true" />
                </Popover.Button>
              </div>
            </div>
            <div className="flex flex-col justify-around h-[200px] w-[90%] mx-auto text-secondary">
              <NavLink to="/">Home</NavLink>
              <NavLink to="/paket-travel">Paket Travel</NavLink>
              <NavLink to="/services">Services</NavLink>
              <NavLink to="/testimonials">Testimonial</NavLink>
            </div>
            {userInfoTravel ? (
              <button
                onClick={logoutHandler}
                className="block w-full bg-white hover:bg-primary py-2 px-4 text-secondary hover:text-white text-center text-xl"
              >
                Log out
              </button>
            ) : (
              <Link
                to="/login"
                className="block w-full bg-white hover:bg-primary py-2 px-4 text-secondary hover:text-white text-center text-xl"
              >
                Login
              </Link>
            )}
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
};

export default Navbar;
