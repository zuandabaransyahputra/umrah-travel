import React from 'react';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import NotificationImportantIcon from '@mui/icons-material/NotificationImportant';

const Message = ({ isError = true, message, color }) => {

    return (
        <div
            className={`w-full text-white px-6 py-4 border-0 rounded relative mb-4`} style={{ backgroundColor: `${color}` }}
        >
            <span className="text-xl inline-block mr-5 align-middle">
                {isError ? (
                    <NotificationImportantIcon fontSize="small" />
                ) : (
                    <NotificationsActiveIcon fontSize="small" />
                )}
            </span>
            <span className="inline-block align-middle mr-8">
                <b className="capitalize">{message}</b>
            </span>
            <button className="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
                <span>×</span>
            </button>
        </div>
    );
};

export default Message;
