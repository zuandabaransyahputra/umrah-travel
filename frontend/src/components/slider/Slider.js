import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
import ArrowRightRoundedIcon from '@mui/icons-material/ArrowRightRounded';
import ArrowLeftRoundedIcon from '@mui/icons-material/ArrowLeftRounded';

const datas = [
  {
    id: 1,
    image: '/images/card-1.jpg',
    tanggal: '12 JUNI 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 2,
    image: '/images/card-2.jpg',
    tanggal: '12 JULI 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 3,
    image: '/images/card-3.jpg',
    tanggal: '12 AGUSTUS 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 4,
    image: '/images/card-4.jpg',
    tanggal: '12 SEPTEMBER 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 5,
    image: '/images/card-4.jpg',
    tanggal: '12 SEPTEMBER 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
  {
    id: 6,
    image: '/images/card-4.jpg',
    tanggal: '12 SEPTEMBER 2022',
    tutor: 'Ustadz Fulan Abu Fulan',
  },
];

const Slider = () => {
  const carousel = useRef();

  const slideLeft = e => {
    e.preventDefault();
    carousel.current.scrollLeft = carousel.current.scrollLeft - 274;
  };

  const slideRight = e => {
    e.preventDefault();
    carousel.current.scrollLeft = carousel.current.scrollLeft + 274;
  };
  return (
    <>
      <button onClick={slideLeft}>
        <ArrowLeftRoundedIcon className="bg-primary w-[24px] h-24[px] rounded-full text-white" />
      </button>
      <div
        ref={carousel}
        className="w-[250px] md:w-[822px] lg:w-[1096px] mx-auto grid grid-flow-col gap-[24px] overflow-x-hidden slider cursor-grab md:cursor-pointer"
      >
        {datas.map(data => (
          <div
            className="flex-1 rounded-md flex flex-col items-center justify-between min-h-[380px] min-w-[250px] bg-cover bg-center p-4"
            style={{
              backgroundImage: `url(${data.image})`,
            }}
            key={data.id}
          >
            <div className="flex flex-col items-center justify-center text-white">
              <p className="text-md lg:text-sm font-[300]">{data.tanggal}</p>
              <h1 className="text-[18px] md:text-[16px] font-bold text-center">
                {data.tutor}
              </h1>
            </div>
            <Link
              to={`/paket-travel/${data.id}`}
              className="bg-primary px-4 py-2 text-white rounded-md font-[400] text-xl md:text-sm"
            >
              Details
            </Link>
          </div>
        ))}
      </div>
      <button onClick={slideRight}>
        <ArrowRightRoundedIcon className="bg-primary w-[24px] h-24[px] rounded-full text-white" />
      </button>
    </>
  );
};

export default Slider;
