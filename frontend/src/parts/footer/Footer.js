import React from 'react';
import { Link } from 'react-router-dom';

const datas = [
  {
    id: 1,
    name: 'Fitur',
    subname: [
      {
        sub1: <Link to="/tentang-kami">Tentang Kami</Link>,
        sub2: <Link to="/paket-travel">Paket Travel</Link>,
        sub3: <Link to="/kerja-sama">Kerja Sama</Link>,
      },
    ],
  },
  {
    id: 2,
    name: 'Social Media',
    subname: [
      {
        sub1: 'Facebook',
        sub2: 'Instagram',
      },
    ],
  },
  {
    id: 3,
    name: 'Koneksi',
    subname: [
      {
        sub1: 'Aceh',
        sub2: 'Indonesia',
        sub3: '0812-1234-5678',
        sub4: 'umrah-travel@support.com',
      },
    ],
  },
];

const Footer = () => {
  return (
    <div className='w-[90%] mx-auto mt-5'>
      <div className="w-full md:w-[90%] mx-auto flex flex-col items-center">
        <div className="w-full mx-auto flex items-start justify-center gap-5 md:justify-around flex-wrap">
          {datas.map(data => (
            <div
              key={data.id}
              className="flex-1 flex flex-col items-start justify-start text-secondary"
            >
              <h1 className="text-xl font-[400] mb-2">{data.name}</h1>
              {data.subname.map((sub, index) => (
                <div className={`text-sm font-[300] text-black`} key={index}>
                  <p>{sub.sub1}</p>
                  <p>{sub.sub2 && sub.sub2}</p>
                  <p>{sub.sub3 && sub.sub3}</p>
                  <p>{sub.sub4 && sub.sub4}</p>
                </div>
              ))}
            </div>
          ))}
        </div>
        <p className="text-center font-[300] text-black my-5">
          2022 Copyright umrah travel - Zuanda Baransyah Putra
        </p>
      </div>
    </div>
  );
};

export default Footer;
