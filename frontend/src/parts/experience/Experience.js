import React from 'react';

const datas = [
  {
    label: 'Members',
    total: '20K',
  },
  {
    label: 'Countries',
    total: '12',
  },
  {
    label: 'Hotels',
    total: '3k',
  },
  {
    label: 'Partners',
    total: '50',
  },
];

const Experience = () => {
  return (
    <div className="flex items-center justify-around w-[90%] md:w-[70%] h-[100px] mx-auto bg-white text-secondary">
      {datas.map((data, index) => (
        <div key={index} className=''>
          <h1 className="text-xl md:text-2xl lg:text-3xl font-bold">{data.total}</h1>
          <p className="text-sm md:text-md lg:text-xl font-[400]">{data.label}</p>
        </div>
      ))}
    </div>
  );
};

export default Experience;
