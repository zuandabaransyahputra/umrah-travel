import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const Hero = () => {
  const userLogin = useSelector(state => state.userLogin);
  const { userInfoTravel } = userLogin;

  return (
    <div
      className="h-screen p-3 bg-cover bg-center flex items-center justify-center flex-col text-white text-center"
      style={{
        backgroundImage: `url(${'./images/bg-home.jpg'
          })`,
      }}
    >
      <h1 className="text-4xl font-bold">
        Umrah Dengan Nyaman <br /> Sesuai Tuntunan Syariat
      </h1>
      <p className="text-xl text-gray-200 my-2 font-[300]">
        Telah dipercaya oleh banyak orang
      </p>
      {!userInfoTravel && (
        <Link to="/register" className="bg-primary p-3 rounded-md mt-2">
          Daftar Sekarang
        </Link>
      )}
    </div>
  );
};

export default Hero;
