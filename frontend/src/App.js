import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import Footer from './parts/footer/Footer';
import Home from './pages/home/Home';
import PaketTravel from './pages/paket-travel/PaketTravel';
import Services from './pages/services/Services';
import Testimonial from './pages/testimonial/Testimonial';
import Login from './pages/login/Login';
import Register from './pages/register/Register';
import DetailPaketTravel from './pages/detailPaket/DetailPaketTravel';
import BookingTiket from './pages/booking/BookingTiket';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/">
          <Route index element={<Home />} />
          <Route path="paket-travel" element={<PaketTravel />} />
          <Route path='paket-travel/:id' element={<DetailPaketTravel />} />
          <Route path='bookings/:id' element={<BookingTiket />} />
          <Route path="services" element={<Services />} />
          <Route path="testimonials" element={<Testimonial />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
        </Route>
      </Routes>
      <footer className="border-t-[1px]">
        <Footer />
      </footer>
    </BrowserRouter>
  );
}

export default App;
