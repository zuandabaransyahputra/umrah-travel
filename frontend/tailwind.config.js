module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    extend: {
      colors: {
        'button': '#ff4948',
        'primary': '#ff9e53',
        'secondary': '#071c4d',
      },
    }
  },
  plugins: [],
}